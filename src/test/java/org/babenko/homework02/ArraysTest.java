package org.babenko.homework02;

import org.junit.Test;
import static org.babenko.homework02.Arrays.*;

public class ArraysTest {

    int arr [] = {4, 8, 9, 6, 7, 2, 1, 3, 5};

    @Test
    public void ArraysTest(){

        System.out.println("Bubble sort");
        bubbleSort(arr);

        System.out.println("\n");

        System.out.println("Selection sort");
        selectionSort(arr);

    }
}
